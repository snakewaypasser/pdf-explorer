package com.snakeway.pdfviewer.listener;

import com.snakeway.pdfviewer.model.TagSectionInfo;

import java.util.List;

public interface OnSearchTagSectionsListener {
    void onProcess();

    void onResult(List<TagSectionInfo> tagSectionInfos);

    void onCancel();
}
