package com.snakeway.pdfviewer.model;

import android.graphics.RectF;

import java.util.List;

/**
 * @author snakeway
 */
public class TagSectionInfo {
    private int start;
    private int end;
    private List<RectF> data;
    private int page;
    private String boundaryBegin;

    private String target;

    private String boundaryEnd;

    public TagSectionInfo() {
    }

    public TagSectionInfo(int start, int end, List<RectF> data, int page) {
        this.start = start;
        this.end = end;
        this.data = data;
        this.page = page;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public List<RectF> getData() {
        return data;
    }

    public void setData(List<RectF> data) {
        this.data = data;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getBoundaryBegin() {
        return boundaryBegin;
    }

    public void setBoundaryBegin(String boundaryBegin) {
        this.boundaryBegin = boundaryBegin;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getBoundaryEnd() {
        return boundaryEnd;
    }

    public void setBoundaryEnd(String boundaryEnd) {
        this.boundaryEnd = boundaryEnd;
    }

    @Override
    public String toString() {
        return "TagSectionInfo{" +
                "start=" + start +
                ", end=" + end +
                ", data=" + data +
                ", page=" + page +
                ", boundaryBegin='" + boundaryBegin + '\'' +
                ", target='" + target + '\'' +
                ", boundaryEnd='" + boundaryEnd + '\'' +
                '}';
    }
}
