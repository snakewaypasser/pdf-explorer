package com.snakeway.pdfviewer.model;

import android.graphics.RectF;

import java.util.List;

/**
 * @author snakeway
 */
public class MarkAreaInfo {
    private int beginIndex;
    private int endIndex;
    private List<RectF> data;
    private int page;
    private String text;

    public MarkAreaInfo() {
    }

    public MarkAreaInfo(int beginIndex, int endIndex, List<RectF> data, int page, String text) {
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
        this.data = data;
        this.page = page;
        this.text = text;
    }

    public int getBeginIndex() {
        return beginIndex;
    }

    public void setBeginIndex(int beginIndex) {
        this.beginIndex = beginIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public List<RectF> getData() {
        return data;
    }

    public void setData(List<RectF> data) {
        this.data = data;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "MarkAreaInfo{" +
                "beginIndex=" + beginIndex +
                ", endIndex=" + endIndex +
                ", data=" + data +
                ", page=" + page +
                ", text='" + text + '\'' +
                '}';
    }
}
