package com.snakeway.pdfviewer.model;

/**
 * @author snakeway
 */
public class PenPoint {
    private float x;
    private float y;

    public PenPoint() {
    }

    public PenPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "PenPoint{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
