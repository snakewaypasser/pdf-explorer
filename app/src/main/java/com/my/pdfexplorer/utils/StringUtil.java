package com.my.pdfexplorer.utils;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

public class StringUtil {

    public static void copyToClipboard(Context context, String content) {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData mClipData = ClipData.newPlainText("", content);
        clipboardManager.setPrimaryClip(mClipData);
    }
}
