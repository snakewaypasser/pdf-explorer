package com.my.pdfexplorer;

import android.app.Application;


public class TheApplication extends Application {

    public static final String TAG = "TheApplication";

    private static TheApplication theApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        theApplication = this;
        init();
    }

    private void init() {

    }

    public static TheApplication getTheApplication() {
        return theApplication;
    }

}
