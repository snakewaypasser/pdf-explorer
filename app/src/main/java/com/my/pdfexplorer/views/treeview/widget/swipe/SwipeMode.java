package com.my.pdfexplorer.views.treeview.widget.swipe;

public enum SwipeMode {
    Single, Multiple
}