package com.my.pdfexplorer.views.third_switch_seekbar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.SeekBar;

import androidx.appcompat.widget.AppCompatSeekBar;


public class ThirdSwitchSeekBar extends AppCompatSeekBar implements AppCompatSeekBar.OnSeekBarChangeListener {

    private SeekTouchListener touchListener;

    //初始值是5 为了解决左右显示不全问题
    private int newProgress = 5;

    public ThirdSwitchSeekBar(Context context) {
        super(context);
        setOnSeekBarChangeListener(this);
        setProgress(5);//三挡开关,为了解决thumb显示不全，所以设为5
    }

    public ThirdSwitchSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnSeekBarChangeListener(this);
        setProgress(5);
    }

    public ThirdSwitchSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOnSeekBarChangeListener(this);
        setProgress(5);
    }


    /**
     * 滑动监听
     */
    public interface SeekTouchListener {
        void touchTop(SeekBar seekBar);//滑到第一档

        void touchMiddle(SeekBar seekBar);//滑到中档

        void touchEnd(SeekBar seekBar);//滑到第三档

    }


    public void setSeekTouchListen(SeekTouchListener touchListener) {
        this.touchListener = touchListener;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        newProgress = progress;
    }


    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (newProgress < 30) {
            newProgress = 5;
            setProgress(5);
            if (touchListener != null) {
                touchListener.touchTop(seekBar);
            }
        } else if (newProgress >= 70) {
            newProgress = 95;
            setProgress(95);
            if (touchListener != null) {
                touchListener.touchEnd(seekBar);
            }
        } else {
            newProgress = 50;
            setProgress(50);
            if (touchListener != null) {
                touchListener.touchMiddle(seekBar);
            }
        }


    }
}
