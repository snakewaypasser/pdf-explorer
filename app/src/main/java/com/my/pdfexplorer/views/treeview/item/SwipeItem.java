package com.my.pdfexplorer.views.treeview.item;


import androidx.annotation.NonNull;

import com.my.pdfexplorer.views.treeview.base.ViewHolder;
import com.my.pdfexplorer.views.treeview.widget.swipe.SwipeItemMangerInterface;
import com.my.pdfexplorer.views.treeview.widget.swipe.SwipeLayout;


public interface SwipeItem {

    int getSwipeLayoutId();

    SwipeLayout.DragEdge getDragEdge();

    void onBindSwipeView(@NonNull ViewHolder viewHolder, int position, SwipeItemMangerInterface swipeManger);

    void openCallback();
}
