package com.my.pdfexplorer.adapter;

import android.annotation.SuppressLint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.my.pdfexplorer.BaseActivity;
import com.my.pdfexplorer.R;
import com.my.pdfexplorer.models.SearchResultItem;
import com.snakeway.pdfviewer.model.TagSectionInfo;

import java.util.ArrayList;
import java.util.List;

public class SearchResultRecyclerAdapter extends RecyclerView.Adapter<SearchResultRecyclerAdapter.SearchResultViewHolder> {
    private BaseActivity baseActivity;
    private List<SearchResultItem> datas;
    private OnSearchResultItemClickListener onSearchResultItemClickListener;

    public SearchResultRecyclerAdapter(BaseActivity baseActivity, List<SearchResultItem> datas, OnSearchResultItemClickListener onSearchResultItemClickListener) {
        if (datas == null) {
            datas = new ArrayList<>();
        }
        this.baseActivity = baseActivity;
        this.datas = datas;
        this.onSearchResultItemClickListener = onSearchResultItemClickListener;
    }

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(baseActivity).inflate(R.layout.activity_main_fragment_search_result_item, parent, false);
        return new SearchResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchResultViewHolder viewHolder, @SuppressLint("RecyclerView") int position) {
        final SearchResultItem searchResultItem = datas.get(position);
        if (searchResultItem.getSearchResultType() == SearchResultItem.SearchResultType.TITLE) {
            viewHolder.linearLayoutTag.setVisibility(View.VISIBLE);
            viewHolder.frameLayoutContent.setVisibility(View.GONE);
            int count = searchResultItem.getChildSearchResultItems() != null ? searchResultItem.getChildSearchResultItems().size() : 0;
            viewHolder.textViewTag.setText(baseActivity.getString(R.string.page_tag) + (searchResultItem.getPage() + 1));
            viewHolder.textViewCount.setText(String.valueOf(count));
        } else {
            viewHolder.linearLayoutTag.setVisibility(View.GONE);
            viewHolder.frameLayoutContent.setVisibility(View.VISIBLE);
            TagSectionInfo tagSectionInfo = searchResultItem.getTagSectionInfo();
            if (tagSectionInfo != null) {
                viewHolder.textViewContent.setText(Html.fromHtml(
                        baseActivity.getResources().getString(R.string.black_red,
                                "..." + searchResultItem.getTagSectionInfo().getBoundaryBegin(),
                                searchResultItem.getTagSectionInfo().getTarget(),
                                searchResultItem.getTagSectionInfo().getBoundaryEnd() + "..."
                        )));
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (onSearchResultItemClickListener != null) {
                            onSearchResultItemClickListener.onItemClick(viewHolder, position, tagSectionInfo);
                        }
                    }
                });
            }
        }

    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public final class SearchResultViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout linearLayoutTag;
        public FrameLayout frameLayoutContent;
        public TextView textViewTag;
        public TextView textViewCount;
        public TextView textViewContent;

        public SearchResultViewHolder(@NonNull View view) {
            super(view);
            linearLayoutTag = (LinearLayout) view.findViewById(R.id.linearLayoutTag);
            frameLayoutContent = (FrameLayout) view.findViewById(R.id.frameLayoutContent);
            textViewTag = (TextView) view.findViewById(R.id.textViewTag);
            textViewCount = (TextView) view.findViewById(R.id.textViewCount);
            textViewContent = (TextView) view.findViewById(R.id.textViewContent);
        }
    }

    public interface OnSearchResultItemClickListener {

        void onItemClick(RecyclerView.ViewHolder viewHolder, int position, TagSectionInfo tagSectionInfo);

    }
}
