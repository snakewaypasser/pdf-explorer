package com.my.pdfexplorer.models;

public class PopupWindowPdfPositionStatus {

    private int x;
    private int y;
    private boolean isUp;

    public PopupWindowPdfPositionStatus() {
    }

    public PopupWindowPdfPositionStatus(int x, int y, boolean isUp) {
        this.x = x;
        this.y = y;
        this.isUp = isUp;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isUp() {
        return isUp;
    }

    public void setUp(boolean up) {
        isUp = up;
    }
}
