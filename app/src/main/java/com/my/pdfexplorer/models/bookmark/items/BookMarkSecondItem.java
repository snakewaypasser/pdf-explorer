package com.my.pdfexplorer.models.bookmark.items;

import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.my.pdfexplorer.R;
import com.my.pdfexplorer.models.bookmark.BookMarkBean;
import com.my.pdfexplorer.views.treeview.base.ViewHolder;
import com.my.pdfexplorer.views.treeview.factory.ItemHelperFactory;
import com.my.pdfexplorer.views.treeview.item.TreeItem;
import com.my.pdfexplorer.views.treeview.item.TreeItemGroup;

import java.util.List;


public class BookMarkSecondItem extends TreeItemGroup<BookMarkBean.BookMarkSecondBean> {

    @Override
    public List<TreeItem> initChild(BookMarkBean.BookMarkSecondBean data) {
        List<TreeItem> items = ItemHelperFactory.createItems(data.childs, this);
        return items;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main_tag_second_item;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder) {
        String remark = data.isRemark ? "(√)" : "";
        viewHolder.setText(R.id.textViewTag, "   " + data.title + remark);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.onBookMarkListener.onItemClick(data);
            }
        });
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, RecyclerView.LayoutParams layoutParams, int position) {
        super.getItemOffsets(outRect, layoutParams, position);
    }
}
