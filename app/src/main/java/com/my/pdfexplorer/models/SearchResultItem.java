package com.my.pdfexplorer.models;

import com.snakeway.pdfviewer.model.TagSectionInfo;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class SearchResultItem {
    public enum SearchResultType {
        TITLE,//用以显示第几页等
        ITEM
    }

    private SearchResultType searchResultType;
    private String tag;
    private int page;

    private TagSectionInfo tagSectionInfo;

    private List<SearchResultItem> childSearchResultItems;


    public SearchResultItem() {
    }

    public SearchResultType getSearchResultType() {
        return searchResultType;
    }

    public void setSearchResultType(SearchResultType searchResultType) {
        this.searchResultType = searchResultType;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public TagSectionInfo getTagSectionInfo() {
        return tagSectionInfo;
    }

    public void setTagSectionInfo(TagSectionInfo tagSectionInfo) {
        this.tagSectionInfo = tagSectionInfo;
    }

    public List<SearchResultItem> getChildSearchResultItems() {
        return childSearchResultItems;
    }

    public void setChildSearchResultItems(List<SearchResultItem> childSearchResultItems) {
        this.childSearchResultItems = childSearchResultItems;
    }

    @NotNull
    public static List<SearchResultItem> convertToSearchResultItems(String tag, List<TagSectionInfo> tagSectionInfos) {
        List<SearchResultItem> searchResultItems = new ArrayList<>();
        if (tagSectionInfos == null) {
            return searchResultItems;
        }
        List<Integer> pages = new ArrayList<>();
        for (TagSectionInfo tagSectionInfo : tagSectionInfos) {
            if (!pages.contains(tagSectionInfo.getPage())) {
                pages.add(tagSectionInfo.getPage());
            }
        }
        for (Integer page : pages) {
            List<SearchResultItem> theSearchResultItems = new ArrayList<>();
            for (TagSectionInfo tagSectionInfo : tagSectionInfos) {
                if (page == tagSectionInfo.getPage()) {
                    SearchResultItem theSearchResultItem = new SearchResultItem();
                    theSearchResultItem.setSearchResultType(SearchResultType.ITEM);
                    theSearchResultItem.setTag(tag);
                    theSearchResultItem.setPage(page);
                    theSearchResultItem.setTagSectionInfo(tagSectionInfo);
                    theSearchResultItems.add(theSearchResultItem);
                }
            }
            SearchResultItem searchResultItem = new SearchResultItem();
            searchResultItem.setSearchResultType(SearchResultType.TITLE);
            searchResultItem.setTag(tag);
            searchResultItem.setPage(page);
            searchResultItem.setTagSectionInfo(null);
            searchResultItem.setChildSearchResultItems(theSearchResultItems);
            searchResultItems.add(searchResultItem);
            searchResultItems.addAll(theSearchResultItems);
        }
        return searchResultItems;
    }
}
