package com.my.pdfexplorer.models;

import com.snakeway.pdfviewer.annotation.PenAnnotation;
import com.snakeway.pdfviewer.annotation.base.BaseAnnotation;

public class ReserveAnnotation {


    private ReserveAnnotationType reserveAnnotationType;

    private BaseAnnotation drawedBaseAnnotation;

    private PenAnnotation drawingPenAnnotation;


    public ReserveAnnotation(ReserveAnnotationType reserveAnnotationType, BaseAnnotation drawedBaseAnnotation, PenAnnotation drawingPenAnnotation) {
        this.reserveAnnotationType = reserveAnnotationType;
        this.drawedBaseAnnotation = drawedBaseAnnotation;
        this.drawingPenAnnotation = drawingPenAnnotation;
    }

    public ReserveAnnotationType getReserveAnnotationType() {
        return reserveAnnotationType;
    }

    public void setReserveAnnotationType(ReserveAnnotationType reserveAnnotationType) {
        this.reserveAnnotationType = reserveAnnotationType;
    }

    public BaseAnnotation getDrawedBaseAnnotation() {
        return drawedBaseAnnotation;
    }

    public void setDrawedBaseAnnotation(BaseAnnotation drawedBaseAnnotation) {
        this.drawedBaseAnnotation = drawedBaseAnnotation;
    }

    public PenAnnotation getDrawingPenAnnotation() {
        return drawingPenAnnotation;
    }

    public void setDrawingPenAnnotation(PenAnnotation drawingPenAnnotation) {
        this.drawingPenAnnotation = drawingPenAnnotation;
    }


}
