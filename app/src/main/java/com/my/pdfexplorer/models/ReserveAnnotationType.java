package com.my.pdfexplorer.models;

public enum ReserveAnnotationType {
    DRAWED,
    DRAWING
}
